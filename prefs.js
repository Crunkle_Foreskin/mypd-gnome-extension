'use strict';

const {GLib, Gio, Gtk} = imports.gi;

// It's common practice to keep GNOME API and JS imports in separate blocks
const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();


// Like `extension.js` this is used for any one-time setup like translations.
function init() {
    log(`initializing ${Me.metadata.name} Preferences`);
}


// This function is called when the preferences window is first created to build
// and return a Gtk widget. As an example we'll create and return a GtkLabel.
function buildPrefsWidget() {
        // Copy the same GSettings code from `extension.js`
    this.settings = ExtensionUtils.getSettings(
        'org.gnome.shell.extensions.mypd');

    // Create a parent widget that we'll return from this function
    let prefsWidget = new Gtk.Grid({
        //margin: 18,
        column_spacing: 12,
        row_spacing: 12,
        visible: true
    });

    // Add a simple title and add it to the prefsWidget
    let title = new Gtk.Label({
        label: `<b>MyPD Preferences</b>`,
        halign: Gtk.Align.START,
        use_markup: true,
        visible: true
    });
    prefsWidget.attach(title, 0, 0, 2, 1);


    let preferences = {
      'mpd-host': "MPD Host (default: localhost):",
      'mpd-port': "MPD Port (default: 6660):",
      'mpc-format': "Format of display:",
    };

    let row = 1;
    Object.keys(preferences).forEach((pref) => {
      // Create a label & switch for `show-indicator`
      let toggleLabel = new Gtk.Label({
          label: preferences[pref],
          halign: Gtk.Align.START,
          visible: true
      });

      prefsWidget.attach(toggleLabel, 0, row, 1, 1);

      let hostValue = this.settings.get_string(pref);
      log(hostValue);
      log(pref);
      let prefField = new Gtk.Entry({
          placeholder_text: hostValue,
      });
      prefField.set_text(hostValue);
      prefField.connect('changed', (widget) => {
        let newText = widget.get_text();
        this.settings.set_string(pref, newText);
      })
      prefsWidget.attach(prefField, 1, row, 1, 1);

      // Bind the switch to the `show-indicator` key
      this.settings.bind(
          pref,
          prefField,
          'active',
          Gio.SettingsBindFlags.DEFAULT
      );

      row++;
    });

    
    // Return our widget which will be added to the window
    return prefsWidget;
}

