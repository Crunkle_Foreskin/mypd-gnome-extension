# MyPD GNOME Extension

MyPD - A simple GNOME extension that displays information about MPD.

**Requirements**

- GNOME 40 >
- MPC must be installed on the host system.
