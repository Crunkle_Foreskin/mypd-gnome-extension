const ByteArray = imports.byteArray;

const St = imports.gi.St;
const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
const Main = imports.ui.main;
const PanelMenu = imports.ui.panelMenu;


function init() {
    log(`initializing ${Me.metadata.name}`);
    this.settings = ExtensionUtils.getSettings('org.gnome.shell.extensions.mypd');
}


function enable() {
        log(`enabling ${Me.metadata.name}`);

        let indicatorName = `${Me.metadata.name} Indicator`;

        // Create a panel button
        this._indicator = new PanelMenu.Button(0.0, indicatorName, false);
        this._songText = null;

        // Add an icon
        //let icon = new St.Label({
        //    gicon: new Gio.ThemedIcon({name: 'face-laugh-symbolic'}),
        //    style_class: 'system-status-icon'
        //});


        // `Main.panel` is the actual panel you see at the top of the screen,
        // not a class constructor.
        Main.panel.addToStatusArea(indicatorName, this._indicator, 0);

        let host = this.settings.get_string('mpd-host');
        let port = this.settings.get_string('mpd-port');
        let format = this.settings.get_string('mpc-format');

        let command = `mpc current -h ${host} -p ${port} -f "${format}"`;
        log(`Running command: ${command}`);

        const addIndicator = () => {
          this._indicator.add_child(this._songText);
        }

        GLib.timeout_add(1, 300, () => {
          try {
            let [, stdout, stderr, status] = GLib.spawn_command_line_sync(command);

            if (status !== 0) {
              if (stderr instanceof Uint8Array)
                stderr = ByteArray.toString(stderr);

                throw new Error(stderr);
              }

              if (stdout instanceof Uint8Array)
                stdout = ByteArray.toString(stdout);

              if (this._songText === null) {
                this._songText = new St.Label({text: stdout, style_class: 'mpd-info'})
                addIndicator()
              } else {
                this._songText.set_text(stdout)
              }

            return true;
          } catch (e) {
            this._indicator.destroy();
            this._indicator = null;
            throw new Error(e);
          }
        });

}


function disable() {
  if (this._indicator !== null) {
    this._indicator.destroy();
    this._indicator = null;
  }
}

